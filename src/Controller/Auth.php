<?php

namespace App\Controller;
use Firebase\JWT\JWT;

class Auth{

	public $manager;
    const SECRET_TOKEN = 'elim5';

	public function servicio()
	{
		return "hola mundo";
	}
    

   public function crearToken($em,$usuario,$email){
        //$em = $this->getDoctrine()->getManager();  


            $data_token=array(
                'username'=>$usuario,
                'email'=>$email,
                'iat'=>time(),
                'exp'=>time()+(7*24*60*60),                                                
            );

            $jwt = JWT::encode($data_token,self::SECRET_TOKEN);
            $decoded = JWT::decode($jwt, self::SECRET_TOKEN, array('HS256'));
            $data=array(
                'success'=>'OK',
                'token'=>$jwt,
                'data'=>$decoded,
            ); 
            return $jwt;

    }


    public function validToken($token){
        $data=null;

        try{ 
            $decoded = JWT::decode($token, self::SECRET_TOKEN, array('HS256'));
        }catch(\Exception $e){
                $data = $e->getMessage();
        }        
        if(isset($decoded)){
            return array('success'=>'OK','data'=>$decoded);
        }else{
            return array('success'=>'ERROR','data'=>$data);
        }

    }

    public function invalidateToken(){
       
    }


}
