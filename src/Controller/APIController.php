<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\Auth;
use App\Entity\User;
use App\Entity\Access;


/**
  * @Route("/api")
 */
class APIController extends AbstractController
{

    /**
     * @Route("/register", methods={"POST"})
     */    
    public function registroAction(Request $request){
        $em = $this->getDoctrine()->getManager();  
        $auth = new Auth();
        $parameters = json_decode($request->getContent(), true);
        $username = $parameters['username'];
        $email = $parameters['email'];
        $success="OK";
        $mensaje="Proceso Correcto";
        $token="";
        $valid = $em->getRepository(User::class)->findOneBy(array('username'=>$username));
        if($valid){
            $success="ERROR";
            $mensaje="Ya te encuentras registrado";
        }else{
            $user = new User();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setAct(0);
            $em->persist($user);
            $em->flush();                
            $token = $auth->crearToken($em,$username,$email);

        }
        return $this->json(['success'=>$success,'token'=>$token,'mensaje'=>$mensaje,'email'=>$email]);
    }

     /**
     * @Route("/login", methods={"POST"})
     */    
    public function loginAction(Request $request){
        $em = $this->getDoctrine()->getManager();  
        $auth = new Auth();
        $parameters = json_decode($request->getContent(), true);
        $username = $parameters['username'];
        $email = $parameters['email'];
        $success="OK";
        $mensaje="Proceso Correcto";
        $token="";
        $valid = $em->getRepository(User::class)->findOneBy(array('username'=>$username,'email'=>$email));
        if(!$valid){
            $success="ERROR";
            $mensaje="Usuario aun no esta registrado";
        }else{
            $token = $auth->crearToken($em,$username,$email);
        }
        return $this->json(['success'=>$success,'token'=>$token,'mensaje'=>$mensaje,'email'=>$email]);

    }

     /**
     * @Route("/valid", methods={"POST"})
     */    
    public function validAction(Request $request){
        $em = $this->getDoctrine()->getManager();  
        $auth = new Auth();
        $parameters = json_decode($request->getContent(), true);
        $token = $parameters['token'];
        $username = $parameters['username'];
        $success="OK";
        $mensaje="Proceso Correcto";

        if(($token==null || $token=="" && $username!=null)){
            $update = $em->getRepository(User::class)->findOneBy(array('username'=>$username));
            $update->setAct(0);
            $update->setToken("");
            $update->setActtoken(0);            
            $em->persist($update);
            $em->flush(); 
            $success="ERROR";
            $mensaje="Acceso Denegado";            
        }else{
            if($username==null && $token!=null){
                $success="ERROR";
                $mensaje="Acceso Denegado"; 
            }else{
                $valid = $auth->validToken($token);
                if($valid['success']=='ERROR'){
                    $success="ERROR";
                    $mensaje="Acceso Denegado";
                    $update = $em->getRepository(User::class)->findOneBy(array('username'=>$username));
                    $update->setToken('');
                    $update->setActtoken(0);
                    $update->setAct(0);
                    $em->persist($update);
                    $em->flush();                     
                }else{
                    $update = $em->getRepository(User::class)->findOneBy(array('username'=>$username));
                    if($update->getAct()==0){
                        $access = new Access();
                        $access->setCreateon(new \DateTime());
                        $access->setUser($update->getId());
                        $em->persist($access);
                        $em->flush();  
                    }
                    $username = $valid['data']->username;
                    $update->setToken($token);
                    $update->setActtoken(1);
                    $update->setAct(1);
                    $em->persist($update);
                    $em->flush(); 

                }        
            }

        }


        return $this->json(['success'=>$success,'token'=>$token,'mensaje'=>$mensaje,'username'=>$username]);

    }    
} 

