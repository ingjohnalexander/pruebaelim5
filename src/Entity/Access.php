<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * Access
 *
 * @ORM\Table(name="access")
 * @ORM\Entity(repositoryClass="App\Repository\AccessRepository")
 */
class Access 
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="user", type="integer")
     */
    private $user;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createon", type="datetime",nullable =true)
     */
    private $createon;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?int
    {
        return $this->user;
    }

    public function setUser(int $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreateon(): ?\DateTimeInterface
    {
        return $this->createon;
    }

    public function setCreateon(?\DateTimeInterface $createon): self
    {
        $this->createon = $createon;

        return $this;
    }




}
