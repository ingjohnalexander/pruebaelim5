<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User 
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    private $username;


    /**
     * @var string
     * @Assert\NotBlank(message="Este dato es obligatorio")     
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var int
     * @ORM\Column(name="act", type="integer", nullable=true)
     */
    private $act;

    /**
     * @var string
     * @ORM\Column(name="token", type="string", length=500, nullable=true)
     */
    private $token;

      /**
     * @var int
     * @ORM\Column(name="acttoken", type="integer", nullable=true)
     */
    private $acttoken;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAct(): ?int
    {
        return $this->act;
    }

    public function setAct(?int $act): self
    {
        $this->act = $act;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getActtoken(): ?int
    {
        return $this->acttoken;
    }

    public function setActtoken(?int $acttoken): self
    {
        $this->acttoken = $acttoken;

        return $this;
    }

}
