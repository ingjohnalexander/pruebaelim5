/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.4.8-MariaDB : Database - elim5prueba
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`elim5prueba` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `elim5prueba`;

/*Table structure for table `access` */

DROP TABLE IF EXISTS `access`;

CREATE TABLE `access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `createon` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access` */

insert  into `access`(`id`,`user`,`createon`) values (1,13,'2021-04-18 04:47:30'),(2,13,'2021-04-18 04:48:19'),(3,13,'2021-04-18 15:00:02'),(4,13,'2021-04-18 15:02:04'),(5,13,'2021-04-18 15:02:38'),(6,13,'2021-04-18 15:03:53'),(7,13,'2021-04-18 15:07:51'),(8,13,'2021-04-18 15:09:25'),(9,13,'2021-04-18 15:19:01'),(10,13,'2021-04-18 15:24:49'),(11,13,'2021-04-18 15:26:29'),(12,13,'2021-04-18 15:27:34'),(13,13,'2021-04-18 15:28:23'),(14,13,'2021-04-18 15:40:10');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `act` int(11) DEFAULT NULL,
  `token` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acttoken` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`email`,`act`,`token`,`acttoken`) values (13,'hello','hello@yahoo.es',1,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImhlbGxvIiwiZW1haWwiOiJoZWxsb0B5YWhvby5lcyIsImlhdCI6MTYxODc2MDc0NiwiZXhwIjoxNjE5MzY1NTQ2fQ.rQeQbte7caxQKkeEC7dfLsca1z4BUaHjGt_0D2nYg7o',1),(15,'pedro','pedro@yahoo.es',0,'',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
